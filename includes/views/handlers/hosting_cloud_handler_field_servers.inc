<?php

/**
 * A handler for the Cloud servers field.
 *
 * @ingroup views_field_handlers
 */
class hosting_cloud_handler_field_servers extends views_handler_field {
  function render($values) {
    $value = $values->{$this->field_alias};
    return hosting_cloud_server_count($value, array(HOSTING_SERVER_ENABLED, HOSTING_SERVER_LOCKED, HOSTING_SITE_QUEUED));
    #return hosting_cloud_server_count();
  }
}

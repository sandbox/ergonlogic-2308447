<?php
/**
 * @file
 *   Hosting cloud views integration.
 */

/**
 * Implements hook_views_data().
 */
function hosting_cloud_views_data() {
  $data['hosting_cloud']['table'] = array(
    'group' => 'Hosting Cloud',
    'title' => 'Cloud',
    'join' => array(
      'node' => array(
        'left_field' => 'vid',
        'field' => 'vid',
      ),
    ),
  );

  $data['hosting_cloud']['vagrant_server'] = array(
    'title' => t('Vagrant Server'),
    'help' => t('Relate a cloud to the vagrant server it is hosted on.'),
    'relationship' => array(
      'handler' => 'views_handler_relationship',
      'base' => 'node',
      'field' => 'vagrant_server',
      'label' => t('vagrant server'),
    ),
  );

  $data['hosting_server']['cloud'] = array(
    'title' => t('Cloud'),
    'help' => t('Relate a server to the cloud of which it is a member.'),
    'relationship' => array(
      'handler' => 'views_handler_relationship',
      'base' => 'node',
      'field' => 'cloud',
      'label' => t('cloud'),
    ),
  );

  $data['hosting_cloud']['publish_path'] = array(
    'title' => t('Publish Path'),
    'help' => t('The path on the server where this cloud is installed.'),
    'field' => array(
      'handler' => 'views_handler_field_xss',
      'click sortable' => TRUE,
    ),
  );

  $data['hosting_cloud']['verified'] = array(
    'title' => t('Verified Date'),
    'help' => t('The most recent date that this cloud was verified.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
  );

  $data['hosting_cloud']['status'] = array(
    'title' => t('Status'),
    'help' => t('The current state of this cloud.'),
    'field' => array(
      'handler' => 'hosting_cloud_handler_field_status',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
  );

  $data['hosting_cloud']['servers'] = array(
    'title' => t('Servers'),
    'help' => t('The number of servers in this cloud.'),
    'field' => array(
      'handler' => 'hosting_cloud_handler_field_servers',
      'field' => 'nid',
    ),
  );

  return $data;
}

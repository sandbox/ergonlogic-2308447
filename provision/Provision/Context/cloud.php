<?php

/**
 * @file Provision named context cloud class.
 */


/**
 * Class for the cloud context.
 */
class Provision_Context_cloud extends Provision_Context {
  public $parent_key = 'server';

  static function option_documentation() {
    return array(
      'root' => 'cloud: path to a vagrant(-pirate) project',
      'server' => 'cloud: drush backend server; default @server_master',
      'vagrant_server' => 'cloud: vagrant server hosting the cloud; default @server_master',
    );
  }

  function init_cloud() {
    $this->setProperty('root');
  }
}

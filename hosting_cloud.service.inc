<?php
/**
 * @file
 *   A cloud service implementation for the hosting front end.
 */

/**
 * The base cloud service type class.
 */
class hostingService_cloud extends hostingService {

  public $service = 'cloud';

  public $has_restart_cmd = FALSE;

  /**
   * node operations
   */

  /**
   * Load associated values for the service.
   *
   * In this vagrant we will use the variable system to retrieve values.
   */
  function load() {
    // REMEMBER TO CALL THE PARENT!
    parent::load();

    $this->mergeData("SELECT cloud FROM {hosting_server} WHERE vid = :vid", array(':vid' => $this->server->vid));
  }

  /**
   * Display settings for adding the cloud element to the server node page.
   *
   * @param
   *   A reference to the associative array of the subsection of the page
   *   reserved for this service implementation.
   */
  function view(&$render) {
    // REMEMBER TO CALL THE PARENT!
    parent::view($render);
    $render['cloud'] = array(
      '#type' => 'item',
      '#title' => t('Cloud'),
      '#markup' => _hosting_node_link($this->cloud),
    );
  }

  /**
   * Extend the server node form to include reference to a cloud.
   *
   * @param
   *   A reference to the associative array of the subsection of the form
   *   reserved for this service implementation.
   */
  function form(&$form) {
    // REMEMBER TO CALL THE PARENT!
    parent::form($form);
    $default_cloud = isset($_GET['cloud']) ? $_GET['cloud'] : NULL;
    $form['cloud'] = array(
      '#type' => 'radios',
      '#title' => t('Cloud'),
      '#description' => t('The cloud on which you want the server to be provisioned.'),
      '#default_value' => isset($this->cloud) ? $this->cloud : $default_cloud,
      '#options' => _hosting_get_clouds(),
      '#weight' => 5,
    );
  }

  /**
   * Validate a form submission.
   */
  function validate(&$node, &$form) {
    // REMEMBER TO CALL THE PARENT!
    parent::validate($node, $form);

/*    if (sizeof($this->vagrant_field) > 30) {
      form_set_error('vagrant_field', t("The vagrant string must not be longer than 30 characters"));
    }//*/
  }

  /**
   * Insert a record into the database.
   *
   * Called by hosting_server_hook_insert().
   *
   * The values associated with this implementation have already
   * been set as properties of $this object, so we now need to
   * save them.
   */
  function insert() {
    // REMEMBER TO CALL THE PARENT!
    parent::insert();
    $id = db_update('hosting_server')
      ->fields(array(
        'cloud' => $this->cloud,
      ))
      ->condition('nid', $this->server->nid)
      ->condition('vid', $this->server->vid)
      ->execute();
  }

  /**
   * Update a record in the database.
   *
   * Called by hosting_server_hook_update().
   */
  function update() {
    // REMEMBER TO CALL THE PARENT!
    parent::update();
    $id = db_update('hosting_server')
      ->fields(array(
        'cloud' => $this->cloud,
      ))
      ->condition('nid', $this->server->nid)
      ->condition('vid', $this->server->vid)
      ->execute();
  }

  /**
   * Delete a record from the database, based on server node.
   */
  function delete() {
    // REMEMBER TO CALL THE PARENT!
    parent::delete();

#    variable_del('hosting_vagrant_field_' . $this->server->nid);
  }

  /**
   * Delete a specific reivision from the database.
   *
   * Not relevant in our vagrant but shown anyway.
   */
  function delete_revision() {
    // REMEMBER TO CALL THE PARENT!
    parent::delete_revision();
  }

  /**
   * Pass values to the provision backend when we call provision-save.
   *
   * By selecting this type we already pass the '--vagrant_service_type=pirate' option
   * to the command, which will load the matching provisionService class in the backend.
   *
   * This backend class will be responsible for receiving and reacting to the options
   * passed here.
   *
   * @ingroup backend-frontend-IPC
   */
  public function context_options($task_type, $ref_type, &$task) {
    // REMEMBER TO CALL THE PARENT!
    parent::context_options($task_type, $ref_type, $task);

    $task->context_options['cloud'] = $this->cloud;
  }
}



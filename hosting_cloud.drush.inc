<?php
/**
 * @file
 *   Implement drush hooks for the Clouds module.
 */

/**
 * Implementation of hook_hosting_TASK_OBJECT_context_options().
 */
function hosting_hosting_cloud_context_options(&$task) {
  $task->context_options['server'] = '@server_master';
  $task->context_options['vagrant_server'] = hosting_context_name($task->ref->vagrant_server);
  // Trim the path so we dont pass extra spaces.
  $task->context_options['root'] = trim($task->ref->publish_path, " ");
}

/**
 * Implements hook_drush_context_import().
 */
function hosting_cloud_drush_context_import($context, &$node) {
  if ($context->type == 'cloud') {
    $node->title = str_replace('cloud_', '', trim($context->name, '@'));
    $node->vagrant_server = hosting_drush_import($context->vagrant_server);
    $node->publish_path = $context->root;
  }
}

/**
 * Implements hook_post_verify().
 *
 * Sets the cloud verified timestamp, to discren when it was verified.
 * Imports all the profiles and modules into package and package release nodes.
 */
function hosting_cloud_post_hosting_verify_task($task, $data) {
  $node = $task->ref;
  if ($node->type == 'cloud') {
    $context = $data['context'];

    // Lock clouds by default
    if ($node->verified == 0 && variable_get('hosting_lock_clouds_by_default', FALSE)) {
      $node->cloud_status = HOSTING_CLOUD_LOCKED;
    }

    $node->verified = REQUEST_TIME; // set verified flag on cloud, to let it know it has been checked.

    /**
     * If we are verifying a Locked cloud (i.e if the publish_path has changed),
     * don't reset the status to Enabled. We don't need to check whether a cloud
     * is deleted here for the same reason, because we don't allow a deleted cloud
     * to be reverified.
     */
    if ($node->cloud_status != HOSTING_CLOUD_LOCKED) {
      $node->cloud_status = HOSTING_CLOUD_ENABLED;
    }
    $node->no_verify = TRUE;
    // Save the cloud being verified
    node_save($node);
  }
}

/**
 * Implements hook_hosting_post_DELETE().
 */
function hosting_cloud_post_hosting_delete_task($task, $data) {
  $task->ref->cloud_status = HOSTING_CLOUD_DELETED;
  $task->ref->no_verify = TRUE;
  node_save($task->ref);
  hosting_context_delete($task->ref->nid);
  db_delete('hosting_cloud_client_access')
    ->condition('pid', $task->ref->nid)
    ->execute();
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function hosting_cloud_post_hosting_lock_task($task, $data) {
  $task->ref->cloud_status = HOSTING_CLOUD_LOCKED;
  $task->ref->no_verify = TRUE;
  node_save($task->ref);
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function hosting_cloud_post_hosting_unlock_task($task, $data) {
  $task->ref->cloud_status = HOSTING_CLOUD_ENABLED;
  $task->ref->no_verify = TRUE;
  node_save($task->ref);
}


<?php
/**
 * @file
 *   Expose the cloud feature to hostmaster.
 */

/**
 * Implementation of hook_hosting_feature().
 */
function hosting_cloud_hosting_feature() {
  $features['cloud'] = array(
    'title' => t('Cloud administration'),
    'description' => t('Create and manage clouds.'),
    'status' => HOSTING_FEATURE_DISABLED,
    'module' => 'hosting_cloud',
    'node' => 'cloud',
    'group' => 'experimental',
    'role_permissions' => array(
      'aegir cloud manager' => array(
        'administer clouds',
        'create cloud',
        'delete cloud',
        'edit cloud',
        'view locked clouds',
        'view cloud',
        'create servers on locked clouds',
      ),
      'aegir client' => array(
        'view cloud',
      ),
    ),

  );
  return $features;
}

